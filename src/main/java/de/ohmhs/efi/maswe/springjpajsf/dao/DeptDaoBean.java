package de.ohmhs.efi.maswe.springjpajsf.dao;

import de.ohmhs.efi.maswe.springjpajsf.model.Department;
import org.springframework.stereotype.Component;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 * implementation if the {@link DeptDao} interface with JPA
 * @author Matthias Roth
 */

// marks this object as Spring Component
@Component
public class DeptDaoBean implements DeptDao {
	
	// injects a JPA entity manager instance into this bean.
	// the entity manager factory is defined in applicationContext.xml
	@PersistenceContext
	private EntityManager em;

	
	/* (non-Javadoc)
	 * @see de.gsohs.efi.maswe.webappjpajsf.dao.DeptDao#findAll()
	 */
	@SuppressWarnings("unchecked")
	public List<Department> findAll() {
		// ask the entity manager to create a dynamic EJBQL query
		Query q = em.createQuery("SELECT d FROM department d");
		// executes the query and returns a list of entities e.g. departments
		return (List<Department>) q.getResultList();
	}

}
