package de.ohmhs.efi.maswe.springjpajsf.model;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;

@Entity(name="region")
@Table(name="REGIONS")
@Getter
@Setter
public class Region {
	
	@Id
	@Column(name="REGION_ID", columnDefinition = "numeric")
	private Long regionId;

	@Size(min=3,max=25,message="string must contain 3 to 25 characters")
	@Column(name="REGION_NAME", length=25)
	private String name;
}