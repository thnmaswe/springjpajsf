package de.ohmhs.efi.maswe.springjpajsf.view;

import de.ohmhs.efi.maswe.springjpajsf.model.Department;
import de.ohmhs.efi.maswe.springjpajsf.model.Employee;
import de.ohmhs.efi.maswe.springjpajsf.service.EmpService;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


import jakarta.faces.component.UIParameter;
import jakarta.faces.event.ActionEvent;
import jakarta.faces.event.ValueChangeEvent;
import jakarta.faces.model.SelectItem;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 * The JSF Backing Bean for this application. Comments and api doc can be seen in webappjsf project
 * 
 * @author Matthias Roth
 */

@Component
@SessionScope
public class EmpView {
	
	private static final Logger LOG = Logger.getLogger(EmpView.class.getName());

	private final EmpService service;
	
	private Integer selectedDepartment;
	private Employee selectedEmployee;


	public EmpView(EmpService empService) {
		this.service = empService;
	}


	public List<Employee> getEmployees() {
		List<Employee> emps;
		if(selectedDepartment != null) {
			emps = service.getEmployeesByDepartment(selectedDepartment);
		} else {
			emps = service.getAllEmployees();
		}
		return emps;
	}

	
	public List<SelectItem> getDepartments() {
		List<SelectItem> items = new ArrayList<>();
		List<Department> depts = service.getAllDepartments();

		for (Department d : depts) {
			items.add(new SelectItem(d.getDepartmentId(),d.getDepartmentName()));
		}
		return items;
	}


	public Integer getSelectedDepartment() {
		return selectedDepartment;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}


	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}


	public void setSelectedDepartment(Integer selectedDepartment) {
		this.selectedDepartment = selectedDepartment;
	}
	
	public void onUpdateDepartment(ValueChangeEvent evt) {
		LOG.info("department with no: "+((Integer)evt.getNewValue()).toString()+" has been selected");
		this.selectedDepartment = (Integer) evt.getNewValue();
	}
	
	public void onEditEmployee(ActionEvent evt) {
		UIParameter component = (UIParameter) evt.getComponent().findComponent("employeeId");
		Integer empNo = Integer.parseInt(component.getValue().toString());
		LOG.info("Employee with No."+empNo.toString()+" has been selected for editing");
		selectedEmployee = service.getEmployeeById(empNo);
	}
	
	public String saveEmployee() {
		if(selectedEmployee != null) {
			service.saveEmployee(selectedEmployee);
		}
		return "employees";
	}
	
}
