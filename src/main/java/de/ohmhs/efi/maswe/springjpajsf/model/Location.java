package de.ohmhs.efi.maswe.springjpajsf.model;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity(name="location")
@Table(name="LOCATIONS")
@Getter
@Setter
public class Location {

	@Id
	@SequenceGenerator(sequenceName="LOCATIONS_SEQ",name="LOCATIONS_SEQ_GEN",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="LOCATIONS_SEQ_GEN")
	@Column(name="LOCATION_ID",scale=4, columnDefinition = "numeric(4,0)")
	private Integer locationId;
	
	@Column(name="STREET_ADDRESS",length=40)
	private String streetAddress;
	
	@Column(name="POSTAL_CODE",length=12)
	private String postalCode;
	
	@Column(name="CITY",length=30,nullable=false)
	private String city;
	
	@Column(name="STATE_PROVINCE",length=25)
	private String stateProvince;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY_ID",referencedColumnName="COUNTRY_ID")
	private Country country;
}
