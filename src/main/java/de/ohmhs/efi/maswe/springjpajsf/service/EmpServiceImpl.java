package de.ohmhs.efi.maswe.springjpajsf.service;

import de.ohmhs.efi.maswe.springjpajsf.dao.DeptDao;
import de.ohmhs.efi.maswe.springjpajsf.dao.EmpDao;
import de.ohmhs.efi.maswe.springjpajsf.model.Department;
import de.ohmhs.efi.maswe.springjpajsf.model.Employee;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Impelementation of the {@link EmpService} interface
 * 
 * @author Matthias Roth
 *
 */
@Transactional(Transactional.TxType.SUPPORTS)
@Component
public class EmpServiceImpl implements EmpService {

	private final EmpDao dao;
	private final DeptDao deptDao;

	public EmpServiceImpl(EmpDao empDao, DeptDao deptDao) {
		this.dao = empDao;
		this.deptDao = deptDao;
	}

	public List<Employee> getAllEmployees() {
		return dao.findAll();
	}
	
	public List<Employee> getEmployeesByDepartment(Integer deptno) {
		return dao.findByDeptNo(deptno);
	}

	public List<Department> getAllDepartments() {
		return deptDao.findAll();
	}

	public Employee getEmployeeById(Integer id) {
		return dao.findById(id);
	}
	// mark this method as transactional. JPA Transaction boundaries are equal to the duration of
	// this business method call. 
	@Transactional(Transactional.TxType.REQUIRED)
	public void saveEmployee(Employee employee) {
		dao.saveEmployee(employee);
	}
	
	

}
