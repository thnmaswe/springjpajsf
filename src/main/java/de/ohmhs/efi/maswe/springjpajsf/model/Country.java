package de.ohmhs.efi.maswe.springjpajsf.model;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity(name="country")
@Table(name="COUNTRIES")
@Getter
@Setter
public class Country {
	
	@Id
	@Column(name="COUNTRY_ID",length=2, columnDefinition = "bpchar(2)")
	private String countryId;
	
	@Column(name="COUNTRY_NAME", length=40)
	private String countryName;
	
	@ManyToOne
	@JoinColumn(name="REGION_ID", referencedColumnName="REGION_ID")
	private Region region;
}