package de.ohmhs.efi.maswe.springjpajsf.dao;

import de.ohmhs.efi.maswe.springjpajsf.model.Department;

import java.util.List;

/**
 * Data Access Object interface for {@link Department} entities
 * @author Matthias Roth
 *
 */
public interface DeptDao {
	/**
	 * 
	 * @return a list of all departments in the database
	 */
	List<Department> findAll();
}
