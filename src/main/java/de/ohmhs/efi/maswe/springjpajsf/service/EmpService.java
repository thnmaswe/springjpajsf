package de.ohmhs.efi.maswe.springjpajsf.service;

import de.ohmhs.efi.maswe.springjpajsf.model.Department;
import de.ohmhs.efi.maswe.springjpajsf.model.Employee;

import java.util.List;
/**
 * defines all business methods of this service fassade.
 * @author Matthias Roth
 *
 */
public interface EmpService {

	Employee getEmployeeById(Integer id);
	List<Employee> getAllEmployees();
	List<Department> getAllDepartments();
	List<Employee> getEmployeesByDepartment(Integer deptno);
	void saveEmployee(Employee employee);
}