package de.ohmhs.efi.maswe.springjpajsf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@SpringBootApplication
@Configuration
public class SpringjpajsfApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringjpajsfApplication.class, args);
    }

    @Bean
    ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource rbms = new ResourceBundleMessageSource();
        rbms.setBasename("messages");
        return rbms;
    }
}
